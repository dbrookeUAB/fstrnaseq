# TCGAseq

<!-- badges: start -->
[![Netlify Status](https://api.netlify.com/api/v1/badges/dcc40d86-7cd2-4a63-9467-844bb413a1de/deploy-status)](https://app.netlify.com/sites/relaxed-joliot-3ba734/deploys)
[![Pipline Status](https://gitlab.com/dbrookeUAB/fstrnaseq/badges/master/build.svg)](https://gitlab.com/dbrookeUAB/fstrnaseq/pipelines)
[![Build Status](https://img.shields.io/gitlab/pipeline/dbrookeUAB/FSTrnaseq)](https://gitlab.com/dbrookeUAB/fstrnaseq)
[![Twitter](https://img.shields.io/twitter/url?style=social&url=https%3A%2F%2Ffstrnaseq.deweybrooke.org%2F)](https://twitter.com/intent/tweet?text=Wow:&url=https%3A%2F%2Fgitlab.com%2FdbrookeUAB%2Ffstrnaseq)
<!-- badges: end -->

<div>
<img src="logo.png" alt="" width="600" />
</div>

This package enables quick retrieval of gene expression data from the entire TCGA RNAseq dataset along with any of available clinical or biological annotations. 

## Installation

You can install the developmental version of TCGAseq from [Gitlab](https://gitlab.com/dbrookeUAB/TCGAseq) with:

``` r
remotes::install_gitlab("dbrookeUAB/TCGAseq")
```

Because this package contains all RNAseq data from the TCGA, the package is very large (~5.5GB) and might take awhile to install. 




