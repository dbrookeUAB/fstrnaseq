# TCGAseq 0.0.2

* Renamed package from `FSTrnaseq` to `TCGAseq` to better differentiate the other `GTEXseq` package. 

# FSTrnaseq 0.0.1

* Added a `NEWS.md` file to track changes to the package.
